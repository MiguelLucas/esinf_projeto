package logic;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.bind.ValidationEvent;
import javax.xml.transform.Templates;

import org.xml.sax.ext.LexicalHandler;

public class FinancasMain {
	private DoublyLinkedList<Cidadao> cidadaos;
	private DoublyLinkedList<Reparticao> reparticoes;
	private DoublyLinkedList<Senha> senhas;
	
	public static void main(String[] args) {
		FinancasMain financas = new FinancasMain();
		
		menu(financas);

	}
	
	public static void menu(FinancasMain financas) {
		System.out.println("#############################");
		System.out.println("           Financas          ");
		System.out.println("#############################");
		
		System.out.println("Escolha uma das opcoes seguintes:");
		System.out.println("1 - Ler ficheiros");
		System.out.println("2 - Adicionar uma reparticao");
		System.out.println("3 - Remover uma reparticao");
		System.out.println("4 - Adicionar um cidadao");
		System.out.println("5 - Servicos mais usados");
		System.out.println("9 - Mostrar todos os dados");
		System.out.println("0 - Sair do programa");
		int opt;
		
		Scanner reader = new Scanner(System.in);
		opt = reader.nextInt();
		
		switch (opt) {
		case 1:
			financas.lerFicheiros();
			break;
		case 2:
			financas.adicionarReparticaoMenu();
			break;
		case 3:
			financas.removerReparticaoMenu();
			break;
		case 4:
			financas.adicionarCidadaoMenu();
			break;
		case 5:
			financas.servicosMaisUsadosMenu();
			break;
		case 9:
			financas.imprimeDados();
			menu(financas);
			break;
		case 0:
			System.exit(0);
		}
	}
	
	public FinancasMain() {
		cidadaos = new DoublyLinkedList<>();
		reparticoes = new DoublyLinkedList<>();
		senhas = new DoublyLinkedList<>();
		
	}
	
	/*
	 * ******************************************************************
	 * ********************** INICIO EX. 1 a) ***************************
	 * ******************************************************************
	 */
	
	public void lerFicheiros() {
		lerCidadaos("fx_cidadaos.txt");
		lerReparticoes("fx_reparti��es.txt");
		lerSenhas("fx_senhas.txt");
		menu(this);
	}
	
	public void lerCidadaos(String ficheiro) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(ficheiro));
			String line;
			while((line = in.readLine()) != null) {
				String[] args = line.split(",");
				Cidadao c1 = new Cidadao(args[0], Integer.parseInt(args[1]), args[2], args[3], Integer.parseInt(args[4]));
				cidadaos.addLast(c1);
			}
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("====  ERRO: N�o foi encontrado o ficheiro \"" + ficheiro + "\" ====");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Ficheiro " + ficheiro + " lido com sucesso.");
	}
	
	public void lerReparticoes(String ficheiro) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(ficheiro));
			String line;
			while((line = in.readLine()) != null) {
				String[] args = line.split(",");
				DoublyLinkedList<String> servicos = new DoublyLinkedList<>();
				for (int i=3;i<args.length;i++)
					servicos.addLast(args[i]);
				Reparticao r1 = new Reparticao(args[0], Integer.parseInt(args[1]), args[2],servicos);
				reparticoes.addLast(r1);
			}
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("====  ERRO: N�o foi encontrado o ficheiro \"" + ficheiro + "\" ====");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Ficheiro " + ficheiro + " lido com sucesso.");
	}

	public void lerSenhas(String ficheiro) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(ficheiro));
			String line;
			while((line = in.readLine()) != null) {
				String[] args = line.split(",");
				Senha s1 = new Senha(Integer.parseInt(args[2]),args[1]);
				senhas.addLast(s1);
			}
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("====  ERRO: N�o foi encontrado o ficheiro \"" + ficheiro + "\" ====");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Ficheiro " + ficheiro + " lido com sucesso.");
	}
	
	/*
	 * ******************************************************************
	 * ************************ FIM EX. 1 a) ****************************
	 * ******************************************************************
	 */
	
	/*
	 * ******************************************************************
	 * ********************** INICIO EX. 1 b) ***************************
	 * ******************************************************************
	 */
	public void adicionarReparticaoMenu() {
		System.out.println("*** Criacao de uma nova reparticao ***\n");
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Indique a cidade da nova reparticao");
		
		String cidade = sc.nextLine();
		
		System.out.println("Indique o numero da nova reparticao");
		int numero;
		do {
		    System.out.println("Por favor indique um n�mero positivo");
		    while (!sc.hasNextInt()) {
		        System.out.println("N�mero inv�lido");
		        sc.next();
		    }
		    numero = sc.nextInt();
		} while (numero <= 0);
		System.out.println("Indique o codigo postal da nova reparticao");
		
		sc.nextLine(); // para consumir \n depois do nextInt
		String codigo_postal = sc.nextLine();
		
		System.out.println("Indique um servico a adicionar a esta reparticao (se nao quiser adicionar mais, introduza \"0\" ");
		String servico = "";
		DoublyLinkedList<String> lista = new DoublyLinkedList<>();
		
		do {
			servico = sc.nextLine();
			ListIterator<String> it = lista.listIterator();
			boolean existe = false;
			while (it.hasNext()) {
				if (servico.equals(it.next())) {
					existe = true;
				}
			}
			
			if (existe) {
				System.out.println("Servico ja existente. Por favor indique novo.");
			} else if (!servico.equals("0")) {
				servico = servico.toUpperCase();
				lista.addLast(servico);
				System.out.println("Adicionado servico " + servico);
			}
			
		} while (!servico.equals("0")); 
		
		Reparticao r1 = new Reparticao(cidade, numero, codigo_postal, lista);
		
		adicionarReparticao(r1);
		
		menu(this);
	}
	
	public void adicionarReparticao(Reparticao r) {
		
		ListIterator<Reparticao> it = reparticoes.listIterator();
		while (it.hasNext()) {
			Reparticao rep = it.next();
			
			if (rep.getNumero_reparticao() == r.getNumero_reparticao()) {
	    		System.out.println("Esta reparticao j� existe. Operacao anulada.");
	    		return;
	    	}
	    	
	    	if (r.getCodigo_postal().length() < 4) {
	    		System.out.println("O codigo postal necessita de ter pelo menos 4 digitos. Operacao anulada.");
	    		return;
	    	}
	    	
	    	String codigo_postal1 = rep.getCodigo_postal().substring(0, 4);
	    	String codigo_postal2 = r.getCodigo_postal().substring(0, 4);
	    	
	    	if (codigo_postal1.equals(codigo_postal2)) {
	    		System.out.println("J� existe uma reparticao com este c�digo postal. Operacao anulada.");
	    		return;
	    	}
		}
		
		reparticoes.addLast(r);
		System.out.println("Reparticao adicionada com sucesso.");
		
		ListIterator<Cidadao> it_cidadaos = cidadaos.listIterator();
		while (it_cidadaos.hasNext()) {
			Cidadao cidadao = it_cidadaos.next();
			if (cidadao.getCodigo_postal().substring(0, 4).equals(r.getCodigo_postal().substring(0,4))) {
				cidadao.setNumero_reparticao(r.getNumero_reparticao());
				System.out.println("Cidadao " + cidadao.getNome() + " com o NIF " + cidadao.getNif() + 
						" teve a sua reparticao alterada para a nova.");
			}
		}
		
	}
	
	/*
	 * ******************************************************************
	 * ************************ FIM EX. 1 b) ****************************
	 * ******************************************************************
	 */
	
	/*
	 * ******************************************************************
	 * *********************** INICIO EX. 1 c) **************************
	 * ******************************************************************
	 */
	
	public void removerReparticaoMenu() {
		System.out.println("*** Remocao de uma nova reparticao ***\n");
		
		Scanner sc = new Scanner(System.in);
		
		int numero;
		
		do {
			System.out.println("Indique o numero da reparticao a remover");
		    while (!sc.hasNextInt()) {
		        System.out.println("N�mero inv�lido");
		        sc.next();
		    }
		    numero = sc.nextInt();
		} while (numero <= 0);
		
		removerReparticao(numero);
		
		menu(this);
	}
	
	public void removerReparticao(int numero_reparticao) {
		int nova_reparticao = -1;
		
		String codigo_postal = null;
		Boolean remove = false;
		Reparticao reparticao = null;
		ListIterator<Reparticao> it_reparticao = reparticoes.listIterator();
		while (it_reparticao.hasNext()) {
			reparticao = it_reparticao.next();
	    	if (reparticao.getNumero_reparticao() == numero_reparticao) {
	    		codigo_postal = reparticao.getCodigo_postal().substring(0,4);
	    		remove = true;
	    		break;
	    	}	
	    }
		
		if (remove) {
			it_reparticao.remove();
			System.out.println("Reparticao n� " + reparticao.getNumero_reparticao() + " situada em " + reparticao.getCidade() + 
    				" com o codigo postal " + reparticao.getCodigo_postal() + " removida com sucesso.");
		} else {
			System.out.println("Reparticao indicada n�o existe. Operacao anulada.");
			return;
		}
		
		ListIterator<Cidadao> it_cidadao = cidadaos.listIterator();
		while(it_cidadao.hasNext()) {
			Cidadao cidadao = it_cidadao.next();
			if (cidadao.getNumero_reparticao() == numero_reparticao) {
				//para descobrir a nova reparticao
				if (nova_reparticao == -1) {
					int min = Integer.MAX_VALUE;
					
					ListIterator<Reparticao> it_reparticao2 = reparticoes.listIterator();
					while(it_reparticao2.hasNext()) {
						Reparticao rep = it_reparticao2.next();
						String codigo_postal_temp = rep.getCodigo_postal().substring(0,4);
						
						int diferenca = Math.abs(Integer.parseInt(codigo_postal) - Integer.parseInt(codigo_postal_temp));
						if (diferenca < min) {
							min = diferenca;
							nova_reparticao = rep.getNumero_reparticao();
						}
					}
				}
				
				cidadao.setNumero_reparticao(nova_reparticao);
				System.out.println("O cidadao " + cidadao.getNome() + " com o NIF " + cidadao.getNif() +
						" foi movido para a reparticao n�" + cidadao.getNumero_reparticao());
			}
		}
	}
	
	/*
	 * ******************************************************************
	 * ************************ FIM EX. 1 c) ****************************
	 * ******************************************************************
	 */
	
	/*
	 * ******************************************************************
	 * *********************** INICIO EX. 1 e) **************************
	 * ******************************************************************
	 */
	
	public void adicionarCidadaoMenu() {
		System.out.println("*** Criacao de um novo cidadao ***\n");
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Indique o nome do novo cidadao");
		String nome = sc.nextLine();
		
		System.out.println("Indique o email do novo cidadao");
		String email = sc.nextLine();
		
		System.out.println("Indique o codigo postal do novo cidadao");
		String codigo_postal = sc.nextLine();
		
		System.out.println("Indique o nif do novo cidadao");
		int nif;
		do {
		    System.out.println("Por favor indique um n�mero positivo");
		    while (!sc.hasNextInt()) {
		        System.out.println("N�mero inv�lido");
		        sc.next();
		    }
		    nif = sc.nextInt();
		} while (nif <= 0);
		
		System.out.println("Indique o n� da reparticao do novo cidadao");
		int reparticao = -1;
		int numero;
		do {
		    System.out.println("Por favor indique um n�mero positivo");
		    while (!sc.hasNextInt()) {
		        System.out.println("N�mero inv�lido");
		        sc.next();
		    }
		    numero = sc.nextInt();
		    
		    ListIterator<Reparticao> it_reparticao = reparticoes.listIterator();
		    while(it_reparticao.hasNext()) {
		    	Reparticao rep = it_reparticao.next();
		    	if (rep.getNumero_reparticao() == numero) {
		    		reparticao = numero;
		    		break;
		    		
		    	}	
		    }
		} while (numero <= 0);
		
		adicionarCidadao(nome, email, codigo_postal, nif, reparticao);
		
		menu(this);
	}
	
	public void adicionarCidadao(String nome, String email, String codigo_postal, int nif, int reparticao) {
		
		if (reparticao == -1) {
			System.out.println("A reparticao indicada n�o existe. Operacao anulada.");
			return;
		}
		
		if (codigo_postal.length() < 4) {
    		System.out.println("O codigo postal necessita de ter pelo menos 4 digitos. Operacao anulada.");
    		return;
    	}
		
		Cidadao c1 = new Cidadao(nome, nif, email, codigo_postal, reparticao);
		cidadaos.addLast(c1);
		
		System.out.println("Cidadao  " + nome + " com o email " + email + ", NIF " + nif +
				", codigo postal " + codigo_postal + " e reparticao " + reparticao + " adicionado com sucesso.");
	}
	
	/*
	 * ******************************************************************
	 * ************************ FIM EX. 1 e) ****************************
	 * ******************************************************************
	 */
	
	/*
	 * ******************************************************************
	 * *********************** INICIO EX. 1 g) **************************
	 * ******************************************************************
	 */
	
	public void servicosMaisUsadosMenu() {
		System.out.println("*** Servicos mais usados ***\n");
		servicosMaisUsados();
		menu(this);
	}
	
	public TreeMap<String, Integer> servicosMaisUsados() {
		
		class MyComparator implements Comparator<String>{
			
			Map<String, Integer> base;
			
			public MyComparator(Map<String, Integer> base) {
		        this.base = base;
		    }

			@Override
			public int compare(String s1, String s2) {
				if(base.get(s1) <= base.get(s2)){
		            return 1;
		        } else {
		            return -1;
		        }
			}
		}
		HashMap<String, Integer> mapa = new HashMap<String, Integer>();
		MyComparator comparator = new MyComparator(mapa);
		TreeMap<String, Integer> mapa_ordenado = new TreeMap<>(comparator);
		
		ListIterator<Senha> it_senha = senhas.listIterator();
		while (it_senha.hasNext()) {
			Senha senha = it_senha.next();
			String codigo = senha.getCodigo();
			
			if (mapa.containsKey(codigo)) {
				mapa.put(codigo, mapa.get(codigo) + 1);
			} else {
				mapa.put(codigo, 1);
			}
		}
		
		mapa_ordenado.putAll(mapa);
		Set<Entry<String, Integer>> senhas = mapa_ordenado.entrySet();
		//senhas. = new MyComparator();
		Iterator<Entry<String, Integer>> it = senhas.iterator();
		while (it.hasNext()) {
			Entry<String, Integer> par = it.next();
			System.out.println("O servico " + par.getKey() + " tem " + par.getValue() + " senhas para hoje.");
		}
		
		return mapa_ordenado;
		
	}
	/*
	 * ******************************************************************
	 * ************************ FIM EX. 1 g) ****************************
	 * ******************************************************************
	 */
	
	/*
	 * ******************************************************************
	 * ********************* FUNCOES AUXILIARES *************************
	 * ******************************************************************
	 */
	public void imprimeDados() {
		
		System.out.println("===== CIDADAOS (" + cidadaos.size() + ")=====\n");
		ListIterator<Cidadao> it_cidadao = cidadaos.listIterator();
		while (it_cidadao.hasNext()) {
			Cidadao cidadao = it_cidadao.next();
			System.out.println(cidadao.getNome() + " || " + cidadao.getNif() + " || " + cidadao.getEmail() + " || " +
					cidadao.getCodigo_postal() + " || " + cidadao.getNumero_reparticao());
		}
		
		System.out.println("\n===== REPARTICOES (" + reparticoes.size() + ")  =====\n");
		ListIterator<Reparticao> it_reparticao = reparticoes.listIterator();
		while (it_reparticao.hasNext()) {
			Reparticao reparticao = it_reparticao.next();
			String str = "";
			str += reparticao.getCidade() + " || " + reparticao.getNumero_reparticao() + " || " +
					reparticao.getCodigo_postal() + " || ";
			
			ListIterator<String> it = reparticao.getServicos().listIterator();
			while(it.hasNext()) {
				str += it.next() + ",";
			}
			
			str = str.substring(0, str.length() - 1);
			
			System.out.println(str);
		}
		
		System.out.println("\n===== SENHAS (" + senhas.size() + ")=====\n");
		ListIterator<Senha> it_senha = senhas.listIterator();
		while(it_senha.hasNext()) {
			Senha senha = it_senha.next();
			System.out.println(senha.getNumero() + " || " + senha.getCodigo());
		}
		
		System.out.println();
	}
	
	public DoublyLinkedList<Cidadao> getCidadaos() {
		return cidadaos;
	}

	public void setCidadaos(DoublyLinkedList<Cidadao> cidadaos) {
		this.cidadaos = cidadaos;
	}

	public DoublyLinkedList<Reparticao> getReparticoes() {
		return reparticoes;
	}

	public void setReparticoes(DoublyLinkedList<Reparticao> reparticoes) {
		this.reparticoes = reparticoes;
	}

	public DoublyLinkedList<Senha> getSenhas() {
		return senhas;
	}

	public void setSenhas(DoublyLinkedList<Senha> senhas) {
		this.senhas = senhas;
	}
	
	public void addReparticao(Reparticao r1) {
		reparticoes.addLast(r1);
	}

}
