package logic;
import java.util.ArrayList;

public class Reparticao {
	private String cidade;
	private int numero_reparticao;
	private String codigo_postal;
	private DoublyLinkedList<String> servicos;
	
	public Reparticao(String cidade, int numero_reparticao, String codigo_postal, DoublyLinkedList<String> servicos) {
		this.cidade = cidade;
		this.numero_reparticao = numero_reparticao;
		this.codigo_postal = codigo_postal;
		this.servicos = servicos;
	}
	
	
	
	

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public int getNumero_reparticao() {
		return numero_reparticao;
	}

	public void setNumero_reparticao(int numero_reparticao) {
		this.numero_reparticao = numero_reparticao;
	}

	public String getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(String codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	public DoublyLinkedList<String> getServicos() {
		return servicos;
	}

	public void setServicos(DoublyLinkedList<String> servicos) {
		this.servicos = servicos;
	}
}
