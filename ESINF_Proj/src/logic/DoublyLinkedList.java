package logic;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author João
 */
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;

public class DoublyLinkedList<E> implements Iterable<E>, Cloneable {

// instance variables of the DoublyLinkedList
    private final Node<E> header;     // header sentinel
    private final Node<E> trailer;    // trailer sentinel
    private int size = 0;       // number of elements in the list
    private int modCount = 0;   // number of modifications to the list (adds or removes)

    /**
     * Creates both elements which act as sentinels
     */
    public DoublyLinkedList() {

        header = new Node<>(null, null, null);      // create header
        trailer = new Node<>(null, header, null);   // trailer is preceded by header
        header.setNext(trailer);                    // header is followed by trailer
    }

    /**
     * Returns the number of elements in the linked list
     *
     * @return the number of elements in the linked list
     */
    public int size() {
        return size;
    }

    /**
     * Checks if the list is empty
     *
     * @return true if the list is empty, and false otherwise
     */
    public boolean isEmpty() {
        return size == 0;
        // return (size>0)? false : true;
    }

    /**
     * Returns (but does not remove) the first element in the list
     *
     * @return the first element of the list
     */
    public E first() {
        return (!isEmpty()) ? header.getNext().getElement(): null;
    }

    /**
     * Returns (but does not remove) the last element in the list
     *
     * @return the last element of the list
     */
    public E last() {
        return (!isEmpty()) ? trailer.getPrev().getElement(): null;
    }

// public update methods
    /**
     * Adds an element e to the front of the list
     *
     * @param e element to be added to the front of the list
     */
    public void addFirst(E e) {
        // place just after the header
        if (isEmpty()) {
            Node node = new Node(e,header,trailer);
            header.setNext(node);
            trailer.setPrev(node);
            size++;
            modCount++;
            //addBetween(e, header, trailer);
        } else {
            addBetween(e, header, header.getNext());
        }
    }

    /**
     * Adds an element e to the end of the list
     *
     * @param e element to be added to the end of the list
     */
    public void addLast(E e) {
        // place just before the trailer
        if (isEmpty()) {
            Node node = new Node(e,header,trailer);
            header.setNext(node);
            trailer.setPrev(node);
            size++;
            modCount++;
        } else {
            addBetween(e, trailer.getPrev(), trailer);
        }
    }

    /**
     * Removes and returns the first element of the list
     *
     * @return the first element of the list
     */
    public E removeFirst() {
        if(isEmpty())
            return null;
        return remove(header.getNext());
    }

    /**
     * Removes and returns the last element of the list
     *
     * @return the last element of the list
     */
    public E removeLast() {
        if(isEmpty())
            return null;
        return remove(trailer.getPrev());
    }

// private update methods
    /**
     * Adds an element e to the linked list between the two given nodes.
     */
    private void addBetween(E e, Node<E> predecessor, Node<E> successor) {
        Node<E> node = new Node(e, predecessor, successor);
        predecessor.setNext(node);
        successor.setPrev(node);
        size++;
        modCount++;
    }

    /**
     * Removes a given node from the list and returns its content.
     */
    private E remove(Node<E> node) {
        E e = node.getElement();
        Node<E> next = node.getNext();
        Node<E> prev = node.getPrev();
        if(next==null){
            next = trailer;
        }
        next.setPrev(prev);
        prev.setNext(next);
        size--;
        modCount++;
        return e;
    }

// Overriden methods        
    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        
        DoublyLinkedList<E> other = (DoublyLinkedList<E>) obj;
        if(this.size != other.size())
            return false;
        Iterator<E> itt = this.iterator();      //iterator for this list
        Iterator<E> ito = other.iterator();     //iterator for the other list
        
        while (itt.hasNext()) {
            if (!itt.next().equals(ito.next())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DoublyLinkedList<E> other = new DoublyLinkedList<>();

        Iterator<E> cloned = this.iterator();

        while (cloned.hasNext()) {
            other.addLast(cloned.next());
        }

        return other;
    }

//---------------- nested DoublyLinkedListIterator class ----------------        
    private class DoublyLinkedListIterator implements ListIterator<E> {

        private DoublyLinkedList.Node<E> nextNode, prevNode, lastReturnedNode; // node that will be returned using next and prev respectively
        private int nextIndex;  // Index of the next element
        private int expectedModCount;  // Expected number of modifications = modCount;

        public DoublyLinkedListIterator() {
            this.prevNode = header;
            this.nextNode = header.getNext();
            lastReturnedNode = null;
            nextIndex = 0;
            expectedModCount = modCount;
        }

        final void checkForComodification() {  // invalidate iterator on list modification outside the iterator
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public boolean hasNext() {
            return nextNode.getNext() != null;
        }

        @Override
        public E next() throws NoSuchElementException {
            checkForComodification();
            try {
                if (hasNext()) {
                    lastReturnedNode = nextNode;
                    prevNode = nextNode;
                    nextNode = nextNode.getNext();
                    nextIndex++;
                    return lastReturnedNode.element;
                }
            } catch (NoSuchElementException e) {
                System.out.println("End of list reached. " + e);
            }

            return prevNode.element;
        }

        @Override
        public boolean hasPrevious() {
            return prevNode.getPrev() != null;
        }

        @Override
        public E previous() throws NoSuchElementException {
            checkForComodification();
            try {
                if (hasPrevious()) {
                    lastReturnedNode = prevNode;
                    nextNode = prevNode;
                    prevNode = prevNode.getPrev();
                    nextIndex--;
                    return lastReturnedNode.element;
                }
            } catch (NoSuchElementException e) {
                System.out.println("End of list reached. " + e);
            }

            return lastReturnedNode.element;
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() throws NoSuchElementException {
            checkForComodification();
            if (this.lastReturnedNode == null || isEmpty()) {
                throw new NoSuchElementException("Not allow to remove a node.");
            }
            if (this.lastReturnedNode.equals(this.prevNode)) {
                this.prevNode = this.prevNode.getPrev();
            } else {
                this.nextNode = this.nextNode.getNext();
            }

            DoublyLinkedList.this.remove(this.lastReturnedNode);

            this.expectedModCount++;
        }

        @Override
        public void set(E e) throws NoSuchElementException {
            if (lastReturnedNode == null) {
                throw new NoSuchElementException();
            }
            checkForComodification();

            lastReturnedNode.setElement(e);
        }

        @Override
        public void add(E e) {
            checkForComodification();

            DoublyLinkedList.this.addBetween(e, this.prevNode, this.nextNode);
            this.prevNode = this.nextNode.getPrev();
            this.expectedModCount++;
        }

    }    //----------- end of inner DoublyLinkedListIterator class ----------

//---------------- Iterable implementation ----------------
    @Override
    public Iterator<E> iterator() {
        return new DoublyLinkedListIterator();
    }

    public ListIterator<E> listIterator() {
        return new DoublyLinkedListIterator();
    }

//---------------- nested Node class ----------------
    private static class Node<E> {

        private E element;      // reference to the element stored at this node
        private Node<E> prev;   // reference to the previous node in the list
        private Node<E> next;   // reference to the subsequent node in the list

        public Node(E element, Node<E> prev, Node<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }

        public E getElement() {
            return element;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setElement(E element) { // Not on the original interface. Added due to list iterator implementation
            this.element = element;
        }

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    } //----------- end of nested Node class ----------

}
