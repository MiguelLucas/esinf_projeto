package logic;

public class Cidadao {
	private String nome;
	private int nif;
	private String email;
	private String codigo_postal;
	private int numero_reparticao;
	
	public Cidadao(String nome, int nif, String email, String codigo_postal, int numero_reparticao) {
		this.nome = nome;
		this.nif = nif;
		this.email = email;
		this.codigo_postal = codigo_postal;
		this.numero_reparticao = numero_reparticao;
	}

	
	
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNif() {
		return nif;
	}

	public void setNif(int nif) {
		this.nif = nif;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(String codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	public int getNumero_reparticao() {
		return numero_reparticao;
	}

	public void setNumero_reparticao(int numero_reparticao) {
		this.numero_reparticao = numero_reparticao;
	}
}
