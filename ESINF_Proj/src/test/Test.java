package test;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Before;

import logic.Cidadao;
import logic.DoublyLinkedList;
import logic.FinancasMain;
import logic.Reparticao;

public class Test {

	FinancasMain financas;
	
	@Before
	public void iniciar() {
		financas = new FinancasMain();
	}
	
	public void lerFicheiros() {
		financas.lerCidadaos("fx_cidadaos.txt");
		financas.lerReparticoes("fx_repartições.txt");
		financas.lerSenhas("fx_senhas.txt");
	}
	
	@org.junit.Test
	public void teste_lerFicheiros() {
		assertEquals(0, financas.getCidadaos().size());
		assertEquals(0, financas.getReparticoes().size());
		assertEquals(0, financas.getSenhas().size());
		lerFicheiros();
		assertEquals(3, financas.getCidadaos().size());
		assertEquals(2, financas.getReparticoes().size());
		assertEquals(4, financas.getSenhas().size());
	}
	
	@org.junit.Test
	public void teste_removerReparticao() {
		lerFicheiros();
		DoublyLinkedList<String> lista = new DoublyLinkedList<>();
		lista.addLast("R");
		Reparticao r1 = new Reparticao("Gaia", 2345, "4300", lista);
		financas.addReparticao(r1);
		
		//remover reparticao sem qualquer cidadao associado
		assertEquals(3, financas.getReparticoes().size());
		financas.removerReparticao(2345);
		
		//remover reparticao com cidadaos associados
		assertEquals(2, financas.getReparticoes().size());
		assertEquals(1234, financas.getCidadaos().first().getNumero_reparticao());
		ListIterator<Reparticao> iterator = financas.getReparticoes().listIterator();
		iterator.next();
		financas.removerReparticao(1234);
		assertEquals(1, financas.getReparticoes().size());
		assertEquals(1235, financas.getCidadaos().first().getNumero_reparticao());
		
		//reparticao inexistente
		financas.removerReparticao(1230);
		assertEquals(1, financas.getReparticoes().size());
	}
	
	@org.junit.Test
	public void teste_adicionarCidadao() {
		assertEquals(0, financas.getCidadaos().size());
		financas.adicionarCidadao("Joao", "joao@mail.com", "4480-456", 123456789, 1235);
		assertEquals(1, financas.getCidadaos().size());
		assertEquals("Joao", financas.getCidadaos().first().getNome());
		assertEquals("4480-456", financas.getCidadaos().first().getCodigo_postal());
	}
	
	@org.junit.Test
	public void teste_servicosMaisUsados() {
		lerFicheiros();
		TreeMap<String, Integer> mapa = financas.servicosMaisUsados();
		Iterator<Entry<String, Integer>> it = mapa.entrySet().iterator();
		Entry<String, Integer> primeira = it.next();
		Entry<String, Integer> segunda = null;
		while (it.hasNext()) {
			segunda = it.next();
			assertTrue(primeira.getValue() >= segunda.getValue() );
		}
		
		assertEquals((Integer)3, primeira.getValue());
		assertEquals((Integer)1, segunda.getValue());

	}

}
